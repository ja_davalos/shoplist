fld 		= null;
launchTID	=  null;
var shopTable;
var order = 0;
var partialMaches = [];
var salesObj = [];

var itemNodes	= {};
var deleteNodes = {};
var salesNodes	= {};
var storeNames = [];
function itemNodeTemplate() { 
	this.name			= "";
	this.desc			= "";
	this.selected		= false;
	this.hideit			= false;
	this.temporary		= false;
	this.DBUpdate		= false;
	this.originalorder 	= -1;
	this.buyingorder 	= -1;
	this.photoid 	 	= -1;
	this.familyid 	 	= -1;
	this.stores			= {};
}
function itemNode(JSONObj,store) {
	this.name			= JSONObj.name;
	this.desc			= JSONObj.desc;
	this.selected		= JSONObj.selected;
	this.hideit			= false;
	this.temporary		= JSONObj.temporary;
	this.DBUpdate		= false;
	this.buyingorder 	= JSONObj.buyingorder;
	this.originalorder	= JSONObj.buyingorder;
	this.photoid 		= JSONObj.photoid;
	this.familyid 		= JSONObj.familyid;
	this.stores			= store;
}
function storeNode(name,items) {
	this.name=name;
	this.exist=false;
	this.items=items;
}
function salesNode(name,items) {
	this.store=name;
	this.items=items;
}
function getJSONObj() {
	var json = findElement("jsonobj").innerText;
	var obj = JSON.parse(json, function (key, value) {
		if (key.length > 0) {
			var items = JSON.parse(value);
			for (var  i=0;i<items.length;i++) {
				var storeObj = {};
				if (itemNodes.hasOwnProperty(items[i].name)) {
					storeObj = itemNodes[items[i].name].stores;
				}
				storeObj[key] = new storeNode(key);
				storeObj[key].exist = true;
				itemNodes[items[i].name] = new itemNode(items[i],storeObj);
				if (storeNames.indexOf(key) == -1) storeNames.push(key);
			}
		}
	});
}
document.onclick = function(ev) {
	if (launchTID != null) clearInterval(launchTID);
	cleanSalesInfo();
}

function setMsg(msg,fldName) {
	fld = findElement(fldName); 
	if (fld != null) 
		launchTID = setInterval('launchJNLP("#")',500);
	return false;
}
function launchJNLP(app) {
	if (fld == null) return;
    fld.style.color = fld.style.color == "red" ? "blue" : "red";
} 
function openPage(pageName) {
	var genPopUp = window.open(pageName,"_parent");
 }
function resetopenPage(pageName) {
	var url = "resetFamily.xhtml";
	var obj = new ajaxObj(url,null);
	obj.ajaxFunc = function(objResp) {
		window.open(pageName,"_parent");
	}
	ajaxRequest(obj); 
 }
function expand(parm,ndx) {
	var el = document.getElementById(parm);
	var plusImg = document.getElementById('plusImg'+ndx);
	var minusImg = document.getElementById('minusImg'+ndx);
	if (el.style.display == "block" || el.style.display == "inherit" ) {
		el.style.display = "none";
		minusImg.style.display = "none";
		plusImg.style.display = "inherit";
	} else {
		el.style.display = "block"; 
		minusImg.style.display = "inherit";
		plusImg.style.display = "none";
	}
}
function removeElement(id) {
	var myNode = findElement(id); 
	if (myNode == null) return;
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}
	var button1 = document.getElementById("button1");
	if ( button1 != null) document.body.removeChild(button1);
	ShopTable = document.getElementById("shopTable");
	if (shopTable == null) return false;

	for (var i=0;i<shopTable.rows.length;i++) {
		shopTable.deleteRow(0);
	}

}
function removedynElements() {
	removeElement("selectList");
	removeElement("checkboxes");
}
function addCheckbox(obj,div3,collect) {
	var checkbox = document.createElement("input"); 
	checkbox.type ="checkbox";
	checkbox.name = "checkedList";
	checkbox.value = obj.name;
	checkbox.onclick = function() {updateSingleItem(checkbox)};
	checkbox.defaultChecked = obj.selected;
	if (collect) checkbox.onchange = collectElements; 
	var label = document.createElement('label')
	label.appendChild(checkbox);
	label.appendChild(document.createTextNode(obj.name));
	div3.appendChild(label);
}
function hiddenCell(obj,div3) {
	var itemName = document.createElement('input')
	itemName.style.visibility = "hidden"; 
	itemName.name = "itemName";
	itemName.value = obj.name;
	div3.appendChild(itemName);
	div3.style.visibility = "hidden";
}
function addRow(mytable,flag,keysSorted) {
	for (var i=0;i<keysSorted.length;i++) {
		var name = keysSorted[i];
		if (itemNodes[name].selected != flag || itemNodes[name].hideit) continue;
		
		var newRow = mytable.insertRow(-1);
		var cell = newRow.insertCell(0);		
		addCheckbox(itemNodes[name],cell,true);
		cell = newRow.insertCell(1);
		cell.innerHTML = "<input type='text' size='5' name='descriptions' value='"+itemNodes[name].desc+"'/>";
		cell = newRow.insertCell(2);

		if (itemNodes[name].photoid == -1) 
			cell.innerHTML = '<label id="view" value="view" width="20" height="20" >';
		else 
			cell.innerHTML = '<input type="image" id="view" value="view" src="includes/images/downloadArrow.jpg" width="20" height="20" onclick="return displayImage(this)">';

		cell = newRow.insertCell(-1);
		cell.innerHTML = '<input type="image" id="upload" value="upload" src="includes/images/uploadArrow.jpg" width="20" height="20" onclick="return UploadImage(this)">';
		cell = newRow.insertCell(-1);
		//hiddenCell(itemNodes[name],cell);
    }
}
function sortOnName(parmNodes,shopTable) {
	keysSorted = Object.keys(parmNodes).sort(function(a,b){return parmNodes[a].name.localeCompare(parmNodes[b].name)})
	addRow(shopTable,true,keysSorted); //checked first
	addRow(shopTable,false,keysSorted); //next not checked
}
function sortOnBuyingOrder(parmNodes,shopTable) {
	
	keysSorted = Object.keys(parmNodes).sort(function(a,b){
			if (parmNodes[a].buyingorder == -1 && parmNodes[b].buyingorder == -1)
				return parmNodes[a].name.localeCompare(parmNodes[b].name)
			if (parmNodes[a].buyingorder < parmNodes[b].buyingorder)
				return -1;
			if (parmNodes[a].buyingorder > parmNodes[b].buyingorder)
				return 1;
			return 0;
		});
	addRow(shopTable,true,keysSorted); //checked first
	keysSorted = Object.keys(parmNodes).sort(function(a,b){return parmNodes[a].name.localeCompare(parmNodes[b].name)})
	addRow(shopTable,false,keysSorted); //next not checked
}
function bldMultipleSelect(title,parmNodes) {
	removedynElements();
	var div0 = document.getElementById("selectList");

	if (div0 == null)  {
		div0 = document.createElement("div"); 
		div0.style.overflow = "auto";
		div0.id = "selectList";
		document.body.appendChild(div0);
	}

	var shopTable = document.createElement("TABLE");
	shopTable.id = "shopTable";
	var header = shopTable.createTHead();
	var row = header.insertRow(0);     
	var cell = row.insertCell(-1);
	cell.innerHTML = "<b>Item name </b>";
	var cell = row.insertCell(-1);
	cell.innerHTML = "<b>description </b>";
	var cell = row.insertCell(-1);
	cell.innerHTML = "<b>display<br/>image</b>";
	var cell = row.insertCell(-1);
	cell.innerHTML = "<b>upload<br/>image</b>";
	
	var div3 = document.createElement("div"); 
	div3.id = "checkboxes";
	div3.className="multiselect";

	var sorter=findElement("alphasort");

	if (sorter.checked) sortOnName(parmNodes,shopTable);
	else sortOnBuyingOrder(parmNodes,shopTable);
	div0.appendChild(shopTable);
}
function initShoppingList(store) {
	document.getElementById("storename").value = store;
	var el=document.getElementById("storeTitle");
	el.innerText = "Full Master list";
	if (store != null && store.length > 0) initStoreList(store);
	else refresh(itemNodes); 
}
function initStoreList(store) {
	if (store == null || store.length == 0) 
			store=document.getElementById("storename").value; 
	var el=document.getElementById("storeTitle");
	if (store != null && store.length > 0) 
		el.innerText = "Store name "+store;
	var parm=document.getElementById("storename"); 
	parm.value = store;
	if (store == null || store.length == 0) 
		refresh(itemNodes);
	else {
		var storeNodes = {};
		for (var name in itemNodes) {
			if (itemNodes.hasOwnProperty(name)) {
				if (itemNodes[name].stores[store] == null) continue;
				if (store.localeCompare(itemNodes[name].stores[store].name) != 0)  continue;
				storeNodes[name] = itemNodes[name];
			}
		}
		refresh(storeNodes);
		getJsonSales(storeNodes,store);
	}
} 
function collectcheckedbox() {
	var el=document.getElementsByName("checkedList");
	var desc=document.getElementsByName("descriptions");
	
	if (el.length == 0) return;
	for (var name in itemNodes) {
		if (itemNodes.hasOwnProperty(name)) {
			itemNodes[name].hideit = false;
		}
    }
	for (var i=0;i<el.length;i++) {
		if (el[i].type != "checkbox") continue;
		if (itemNodes[el[i].value].desc.localeCompare(desc[i].value) != 0 || el[i].checked)
			itemNodes[el[i].value].DBUpdate = true;	
		if (itemNodes[el[i].value].selected != el[i].checked)
			itemNodes[el[i].value].DBUpdate = true;	

		itemNodes[el[i].value].desc = desc[i].value;  
		itemNodes[el[i].value].selected = el[i].checked
	}
}
function refresh(parmNodes) {
	collectcheckedbox();
	if (parmNodes == null) parmNodes = itemNodes;
	var el = document.getElementById("chooseItem").value.toUpperCase();
	for (var name in parmNodes) {
		parmNodes[name].hideit = false;
	}
	if (el.length > 0)
		for (var name in parmNodes) {
			if (parmNodes.hasOwnProperty(name)) {
				if (parmNodes[name].name.toUpperCase().indexOf(el) == -1)	//removed  && !parmNodes[name].selected
					parmNodes[name].hideit = true;
			}
		}
	bldMultipleSelect("tmpList",parmNodes);
	return false;
}
function refreshAdminTbl(parm1,parm2) {
	var el = document.getElementById(parm1).value.toUpperCase();
	for (var name in itemNodes) {
		if (itemNodes.hasOwnProperty(name)) {
			if (itemNodes[name].name.toUpperCase().indexOf(el) == -1 ) 
				itemNodes[name].hideit = true;
			else
				itemNodes[name].hideit = false;
		}
	}
	var adminTable = findElement(parm2);
	var savedRow = adminTable.getElementsByTagName('tr')[0];
	removeElement(parm2);
	adminTable.appendChild(savedRow);
	var keysSorted = Object.keys(itemNodes).sort(function(a,b){return itemNodes[a].name.localeCompare(itemNodes[b].name)});
	for (var i=0;i<keysSorted.length;i++) {
		if (itemNodes[keysSorted[i]].hideit) continue;
		adminRow(adminTable,itemNodes[keysSorted[i]]);			
	}
	return false;
}
function addAdminRow(adminTable) {
	var newRow = adminTable.insertRow(-1);
	var cell = newRow.insertCell(0);
	var itemName = document.createElement("input");
	itemName.name = "itemname";
	cell.appendChild(itemName); 		
	for (var i=1;i<adminTable.rows[0].cells.length;i++) {
		var text = adminTable.rows[0].cells[i].innerText.trim();
		var obj = new itemNodeTemplate();
		obj.name = text;
		cell = newRow.insertCell(-1);
		addCheckbox(obj,cell);
	}
}
function collectDeleteElements(itemName,store,checked) {
	if (deleteNodes.hasOwnProperty(itemName)) {
		deleteNodes[itemName].stores[store] = new storeNode(store);
		storesObj = deleteNodes[itemName].stores;
	} else {
		deleteNodes[itemName] = new itemNodeTemplate();
		deleteNodes[itemName].stores[store] = new storeNode(store);
	}
	deleteNodes[itemName].name = itemName;
	deleteNodes[itemName].stores[store].name = store;
	deleteNodes[itemName].stores[store].exist = checked;
}
function collectElements(evt) {
	var trTag = evt.target.parentNode.parentNode.parentNode;
	var itemName = trTag.firstChild.firstChild.value;
	if (itemName != null)
		collectDeleteElements(itemName,evt.currentTarget.value,evt.currentTarget.checked);
	else {
		itemName = evt.currentTarget.value;
		
		if (itemNodes.hasOwnProperty(itemName)) {
			if (!evt.currentTarget.checked)
				itemNodes[itemName].buyingorder = order;
		}
		order = order + 1;
	}
}
function adminRow(adminTable,itemObj,deleteFlag) {
	var newRow = adminTable.insertRow(-1);
	var cell = newRow.insertCell(0);
	var itemName = document.createElement("input");
	itemName.name = "itemname";
	itemName.value = itemObj.name; 
	itemName.readOnly = true; 
	
	cell.appendChild(itemName); 		
	for (var i=1;i<adminTable.rows[0].cells.length;i++) {
		var obj = new itemNodeTemplate();
		obj.name = adminTable.rows[0].cells[i].innerText;
		for (var name in itemObj.stores) {
			if (obj.name == name) {
				obj.selected = itemObj.stores[name].exist;
				break;
			}
		}
		cell = newRow.insertCell(i);
		addCheckbox(obj,cell,true);
		if (!obj.selected && deleteFlag) {
			cell.firstChild.innerText = "";
			continue;
		}
	}
}
/* check if function still used */
function bldItemRow(parm) {
	var adminTable = document.getElementById(parm);
	for (var i=0;i<5;i++) {
		addAdminRow(adminTable);
	}
}
function bldDeleteItems(parm) {
	getJSONObj();
	var adminTable = document.getElementById(parm);
	var keysSorted = Object.keys(itemNodes).sort(function(a,b){return itemNodes[a].name.localeCompare(itemNodes[b].name)});
	for (var i=0;i<keysSorted.length;i++) {
		adminRow(adminTable,itemNodes[keysSorted[i]],true);			
	}
}
function bldUpdateItems(parm) {
	getJSONObj();
	var adminTable = document.getElementById(parm);
	var keysSorted = Object.keys(itemNodes).sort(function(a,b){return itemNodes[a].name.localeCompare(itemNodes[b].name)});
	for (var i=0;i<keysSorted.length;i++) {
		adminRow(adminTable,itemNodes[keysSorted[i]],false);			
	}
}
function tableAsJSON(parm,jsonid) {
	var wrkTable = document.getElementById(parm);
	var jsonNodes	= [];
	for (var i=1;i<wrkTable.rows[0].cells.length;i++) {
		var storeName = wrkTable.rows[0].cells[i].innerText;
		var items		= [];
		for (var j=1;j<wrkTable.rows.length;j++) {
			var cell = wrkTable.rows[j].cells[i].firstChild;
			if (cell.firstChild.type != "checkbox" || !cell.firstChild.checked) continue;
			var value = wrkTable.rows[j].cells[0].firstChild.value.trim();
			if (value.length == 0) continue;
			items.push(wrkTable.rows[j].cells[0].firstChild.value);
		}
		if (items.length == 0) continue;
		jsonNodes.push(new storeNode(storeName,items));
	}	
	if (jsonNodes.length == 0) return false;
	
	var myJSON = JSON.stringify(jsonNodes);
	var jsonArray = document.getElementById(jsonid); 
	jsonArray.value = myJSON;
	return true;
}
function JSONFullTable(parm,jsonid) {
	var jsonNodes	= [];
	for (var name in deleteNodes) {
			var storeArray = []; 
			for (var store in deleteNodes[name].stores) {
				storeArray.push(deleteNodes[name].stores[store]);
			}
			deleteNodes[name].stores = storeArray;
			jsonNodes.push(deleteNodes[name]);
    }
	if (jsonNodes.length == 0) return false;
	
	var myJSON = JSON.stringify(jsonNodes);
	var jsonArray = document.getElementById(jsonid); 
	jsonArray.value = myJSON;
	return true;
}
function bldJSON(jsonid) {
	var jsonNodes	= [];
	collectcheckedbox();
	for (var name in itemNodes) {
		if (itemNodes[name].DBUpdate || itemNodes[name].temporary)
			jsonNodes.push(itemNodes[name]);
    }
	if (jsonNodes.length == 0) return false;
	
	var myJSON = JSON.stringify(jsonNodes);
	var jsonArray = document.getElementById(jsonid); 
	jsonArray.value = myJSON;
	return true;
}
function updateSingleItem(item) {
	var jsonNodes	= [];
	itemNodes[item.value].selected = item.checked;
	jsonNodes.push(itemNodes[item.value]);
	var encoded = encodeURIComponent(JSON.stringify(jsonNodes));
	var url = "ajaxs/getUpdateSingleItem.xhtml?jsonarray="+encoded+"&listname="+document.getElementById('listname').value;
	var obj = new ajaxObj(url,null);
	obj.ajaxFunc = function(objResp) {
	}
	ajaxRequest(obj); 
}
function alphaSort() {
	initStoreList(document.getElementById("storename").value);
}
function popUp(url) {
	genPopUp = window.open(url,"pop","toolbar=no,location=no,scrollbars=yes,directories=no,status=yes,menubar=no,resizable=yes,width=500,height=350");
	genPopUp.focus();
}
function UploadImage(parm) {
	var trTag = parm.parentNode.parentNode;
	var itemName = trTag.firstChild.innerText.replace(/^\s+|\s+$/g, '');
	var jsonNode = new itemNode(itemNodes[itemName]); 

	openPage("loadPhotos.xhtml?jsonitem="+encodeURIComponent(JSON.stringify(jsonNode)));
	return false;
}
function displayImage(parm) {
	var trTag = parm.parentNode.parentNode;
	var itemName = trTag.firstChild.innerText.replace(/^\s+|\s+$/g, '');
	var jsonNode = new itemNode(itemNodes[itemName]); 
	var ndx = window.document.baseURI.lastIndexOf("/");
	var url = window.document.baseURI;
	if (ndx != -1 ) url = url.substring(0,ndx);

	popUp(url+"/image/displayPhotos.xhtml?jsonitem="+encodeURIComponent(JSON.stringify(jsonNode)));
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function setjsonitem() {
	var value = getParameterByName('jsonitem');
	var el = document.getElementById("jsonitem");
	if (el != null) el.value = value;
}
function getJsonSales(storeNodes,store) {
	var url = "ajaxs/ajaxGetJsonSales.xhtml?store="+store;
	var el2 = document.createElement("div"); 
	var obj = new ajaxObj(url,el2);
	obj.ajaxFunc = function(objResp) {
		objResp.ajaxDestEl.innerHTML = objResp.ajaxmsg;
		var json = objResp.ajaxDestEl.innerText;
		var dummy = JSON.parse(json, function (key, value) {
			if (key.length > 0) {
				var items = JSON.parse(value);
				salesNodes[key] = new salesNode(key,items);
			}
		});
		compareSales(storeNodes,store);
	}
	ajaxRequest(obj);
} 
function cleanSalesInfo(salesTable) {
	/*findElement("infoMessage").innerText = "";*/
	var el = findElement("salesbutton");
	if (el != null) el.style="display:none";
	salesObj = [];
	if (salesTable == null) return false;
	var len = salesTable.rows.length;
	for (var i=0;i<len;i++) {
		salesTable.deleteRow(0);
	}
	return false;
}
function showSalesInfo() {
	document.getElementById("salesTable").style.display="block";
	return false;
}
function addSalesRows(table) {
	var newRow = table.insertRow(-1);
	var cell = newRow.insertCell(-1);		
	cell.innerHTML = "<b>Sales List</b>";
	for (var i=0;i<salesObj.length;i++) {		
		newRow = table.insertRow(-1);
		cell = newRow.insertCell(-1);
		cell.innerHTML = salesObj[i].name;
    }
	var newRow = table.insertRow(-1);
	cell = newRow.insertCell(-1);		
	var cmd = 'document.getElementById("salesTable").style.display="none"';
	cell.innerHTML = "<input type='button' id='endView' style='float: right;' onclick='"+cmd+"' value='remove' />";
}
function bldSalesPanel(salesTable) {
	findElement("salesbutton").style="display:block";
	addSalesRows(salesTable);
	salesTable.style="display: none;z-index: 2;position: absolute;background-color: yellow;";
}
function compareSales(storeNodes,store) {
	var salesTable = findElement("salesTable");
	cleanSalesInfo(salesTable);
	salesTable.className="center";

	if ((typeof salesNodes[store] == 'undefined') || (salesNodes[store].items == null)) return;
	var salesItems = salesNodes[store].items;
	for (var i=0;i<salesItems.length;i++) {
		compareToStores(storeNodes,salesItems[i])
	}
	if (salesObj.length > 0) bldSalesPanel(salesTable);

	if (launchTID != null) clearInterval(launchTID);
	setMsg(store+" has some sales that match your list","infoMessage");
}
function compareToStores(storeNodes,saleObj) {
	saleWords = saleObj.name.split(" ");
	for (var name in storeNodes) {
		var storeWords = name.split(" ");
		var ctr = 0;
		for (var i=0;i<saleWords.length;i++) {
			for (var j=0;j<storeWords.length;j++) {
				if (saleWords[i].toUpperCase() === storeWords[j].toUpperCase()) ctr++;
			}
			if (ctr == storeWords.length) {
				salesObj.push(saleObj);
			} else if (ctr > 0) partialMaches.push(saleObj);
		}
	}
}
function bldTempItem() {
	var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function addTempItem(parm) {
	var modal = document.getElementById('myModal');
	if (parm.value.trim().length == 0 || itemNodes.hasOwnProperty(parm.value)) {
			alert("item "+parm.value+" already exist");
			modal.style.display = "none";
			return;
	}
	var familyId = -2;
	for (var name in itemNodes) {
		familyid = itemNodes[name].familyid;
		break;
	}

	var obj = new itemNodeTemplate();
	obj.name = parm.value;
	obj.selected = true;
	obj.temporary = true;
	obj.DBUpdate  = true;
	obj.familyid  = familyid;
	for (var i=0;i<storeNames.length;i++) {
		obj.stores[storeNames[i]] = new storeNode(storeNames[i]);
	}
	itemNodes[obj.name] = obj;
	refresh(itemNodes);
	modal.style.display = "none";
}
function bldItemTable() {
	var parm = document.getElementById('storelist');
	var storeArray = parm.value.split('\n');
	var modal = document.getElementById('myModal');
	var storeTbl = document.getElementById('storeTbl');
    modal.style.display = "block";
	
	removeElement("storeTbl");
	var header = storeTbl.createTHead();
	var row = header.insertRow(0);     
	var cell = row.insertCell();
	cell.innerHTML = "Item name";
	for (var i=0;i<storeArray.length;i++) {
		cell = row.insertCell();
		cell.innerHTML = storeArray[i];
	}
	var obj = new itemNodeTemplate();
	for (var i=0;i<10;i++) {
		row = storeTbl.insertRow();
		cell = row.insertCell();
		cell.appendChild(document.createElement("input"));
		for (var j=0;j<storeArray.length;j++) {
			cell = row.insertCell();
			obj.name = storeArray[j];
			addCheckbox(obj,cell,true);
		}
	}
	return false;
}
function exitTask() {
	var modal = document.getElementById('myModal');
    modal.style.display = "none";
	return false;
}
function addShoppingList() {
	tableAsJSON('storeTbl','jsonarray');
	var encoded = encodeURIComponent(JSON.stringify(document.getElementById('jsonarray').value));
	var url = "ajaxs/addItemList.xhtml?jsonarray="+encoded;
	var obj = new ajaxObj(url,null);
	obj.ajaxFunc = function(objResp) {
		window.open(pageName,"_parent");
	}
	ajaxRequest(obj); 
}