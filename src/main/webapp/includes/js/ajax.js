ajaxObjs = {};
function ajaxObj(name,el) {
	this.ajaxURL     = name;
	this.ajaxDestEl  = el;
	this.ajaxDone    = false;
	this.ajaxError   = false;
	this.ajaxFunc    = null;
	this.ajaxmsg;
}
function ajaxRequest(obj) {
	ajaxServerReq(obj); 
	return false;	
}
function ajaxServerReq(obj) {
	var pageRequest;
	if (window.XMLHttpRequest) pageRequest = new XMLHttpRequest();
	else if (window.ActiveXObject)  pageRequest = new ActiveXObject("Microsfot.XMLHTTP"); 
		 else
			return;
	
	pageRequest.onreadystatechange = function() {
		if (this.readyState == 4 ) {
			if (this.status != 200) obj.ajaxError = true;
			serverGetData(this.responseText,obj);
		}
	};
	pageRequest.open('GET',obj.ajaxURL,true);
	pageRequest.send();
}
function serverGetData(msg,obj) {
	obj.ajaxDone  = true;
	obj.ajaxmsg = msg;
	if (obj.ajaxFunc != null)
		obj.ajaxFunc(obj);
}