package com.assoc.jad.lbinstance;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class CollectSystemData implements Runnable {
	
	private LoadBalancerObj lbObj;

	public CollectSystemData(LoadBalancerObj lbObj) {
		this.lbObj = lbObj;
	}

	@Override
	public void run() {
		selectOSData();
		new Thread(new WebSocketClient(lbObj),lbObj.getInstance()).start();
	}

	private void populateLoadBalancerObj(Object value,String methodName) {
		Method[] methods = lbObj.getClass().getDeclaredMethods();
		Object[] arguments = new Object[] {value};
		
		methodName = methodName.replaceFirst("get", "set");
		for (int i=0;i<methods.length;i++) {
			if (!methods[i].getName().equals(methodName)) continue;
			
			try {
				methods[i].invoke(lbObj,arguments);
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
    }
	private void selectOSData() {
		//String[] OSName = System.getProperty("os.name").split("\\s+");

		OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
		for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
			method.setAccessible(true);
			if (method.getName().startsWith("get") && Modifier.isPublic(method.getModifiers())) {
				Object value;
				try {
					value = method.invoke(operatingSystemMXBean);
					populateLoadBalancerObj(value,method.getName());
				} catch (Exception e) {
					value = e;
				}
				System.out.println(method.getName() + " = " + value);
			}
		}
	}
}
