package com.assoc.jad.lbinstance;

import java.util.HashMap;
import java.util.Map;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;

/**
 * add the following phase listener to the instance that needs to be load balancer needs to be aware of.<br>
 * This should be added to file <b>faces-config.xml</b><br>
 *	<b>&#60;lifecycle&#62;<br>
 *  	&#60;phase-listener>com.assoc.jad.lbinstance.LifeCycleListener&#60;/phase-listener><br>
 *	&#60;/lifecycle><br></b>
 * @author jorge
 *
 */
public class LifeCycleListener implements PhaseListener {

	private static final long serialVersionUID = 1L;
	public static HashMap<String,LoadBalancerObj> hashMapLoadBalancerObj = new HashMap<String, LoadBalancerObj>();
	
	private long phaseBefore = 0;

	@Override
	public void afterPhase(PhaseEvent event) {
		HttpServletRequest req = (HttpServletRequest)event.getFacesContext().getExternalContext().getRequest();
		String sessionId = req.getSession().getId();
		String script = req.getServletPath();
		bldLoadBalancerObj(event,sessionId+script,sessionId);
	}
	/**
	 * set begin time and build base object using sessionId
	 */
	@Override
	public void beforePhase(PhaseEvent event) {
		phaseBefore = System.currentTimeMillis();
		HttpServletRequest req = (HttpServletRequest)event.getFacesContext().getExternalContext().getRequest();
		String sessionId = req.getSession().getId();
		bldLoadBalancerObj(event,sessionId,sessionId);
	}
	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RENDER_RESPONSE;
	}
	private void bldLoadBalancerObj(PhaseEvent event,String objKey,String sessionKey) {
		long beginTime = System.currentTimeMillis();
		LoadBalancerObj lbObj = hashMapLoadBalancerObj.get(objKey);
		if ( lbObj == null && !objKey.equals(sessionKey)) lbObj = hashMapLoadBalancerObj.get(sessionKey);
		if ( lbObj == null) lbObj = new LoadBalancerObj();
		
		lbObj.setProcessTime(System.currentTimeMillis()-phaseBefore);
		lbObj.setViewId(event.getFacesContext().getViewRoot().getViewId());
		lbObj.setInstancePort(event.getFacesContext().getExternalContext().getRequestServerPort());
		lbObj.setServerName(event.getFacesContext().getExternalContext().getRequestServerName());
		lbObj.setSchema(event.getFacesContext().getExternalContext().getRequestScheme());
		Map<String,String> parms = event.getFacesContext().getExternalContext().getRequestParameterMap();
		String loadBalancerWSURL = parms.get("loadBalancerWSURL");
		if (loadBalancerWSURL != null) lbObj.setLoadBalancerWSURL(loadBalancerWSURL);
		String application = parms.get("application");
		if (application != null) lbObj.setInstance(application);
		lbObj.setNetworkBgnTime(beginTime);

		hashMapLoadBalancerObj.put(objKey,lbObj);
	}
}
