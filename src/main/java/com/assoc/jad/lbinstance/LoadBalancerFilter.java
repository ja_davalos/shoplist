package com.assoc.jad.lbinstance;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * 
 * this JSF filter allows http request to be modified (line added) to include a
 * script line. must be aware of 2 ways to output; via <b>getWriter</b> for html
 * text and <b>getOutputStream</b> for everything else.<br>
 * first execute <b>chain.doFilter(request, wrapper)</b>;<br>
 * next if ( wrapper.output.size() == 0) goto <b>wrapperIO</b> that executes
 * getOutputStream.<br>
 * otherwise add the <b>include</b> line
 *
 * @author jorge
 */
public class LoadBalancerFilter implements Filter {
	private String javascript = System.lineSeparator() + "<script type=\"text/javascript\" src='%s'></script>" + System.lineSeparator();

	public class CharResponseWrapper extends HttpServletResponseWrapper {
		private CharArrayWriter output;

		public String toString() {
			return output.toString();
		}

		public CharResponseWrapper(HttpServletResponse response) {
			super(response);
			output = new CharArrayWriter();
		}

		public PrintWriter getWriter() {
			return new PrintWriter(output);
		}

		public void setOutput(CharArrayWriter output) {
			this.output = output;
		}
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		this.javascript = String.format(javascript, filterConfig.getInitParameter("script"));
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		LoadBalancerFilterIO wrapperIO = new LoadBalancerFilterIO();
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		if (resp.isCommitted()) return;
		
		long beginTime = 0;
		String sessionId = req.getSession().getId();
		if (req.getQueryString() != null && 
			req.getServletPath().equals("/loadEndNetworkTime"))
			setEndTime(sessionId,req);

		CharResponseWrapper wrapper = new CharResponseWrapper((HttpServletResponse) response);
		chain.doFilter(request, wrapper);
		// do after "chain" because it forces the life cycle class to run.
		LoadBalancerObj lbObj = LifeCycleListener.hashMapLoadBalancerObj.get(sessionId+req.getServletPath());
		if (lbObj != null ) beginTime = lbObj.getNetworkBgnTime();
		String networkBgnTime = String.format(
				"\t<input type=\"text\" value=\"%d\" id=\"networkBgnTime\" name=\"networkBgnTime\"/>\r\n", beginTime);
		String pagename = String.format(
				"\t<input type=\"text\" value=\"%s\" id=\"script\" name=\"script\"/>\r\n", req.getRequestURI());
		int size = -1;
		if ((size = wrapperOK(wrapper)) > 0) {
			CharArrayWriter caw = new CharArrayWriter();
			caw.write(wrapper.toString().substring(0, size));
			if (wrapper.toString().indexOf("</head>") != -1) caw.write(javascript);
			caw.write(networkBgnTime);
			caw.write(pagename);
			
			caw.write(wrapper.toString().substring(--size));
			response.setContentLength(caw.toString().length());
			response.getWriter().write(caw.toString());
		} else {
			if (wrapper.output.size() == 0)
				wrapperIO.doFilter(request, response, chain);
			else {
				response.getWriter().write(wrapper.toString());
			}
		}
	}
	private void setEndTime(String sessionId,HttpServletRequest req) {
		String script = "";
		String networkEndTime = "";
		String[] keyValues = req.getQueryString().split("&");
		for (int i=0;i<keyValues.length;i++) {
			int ndx = keyValues[i].indexOf('=');
			if (keyValues[i].startsWith("script")) script = keyValues[i].substring(++ndx);
			if (keyValues[i].startsWith("networkEndTime")) networkEndTime = keyValues[i].substring(++ndx);
		}
		LoadBalancerObj lbObj = LifeCycleListener.hashMapLoadBalancerObj.get(sessionId+script);
		if (lbObj.getInstance() == null ) return; //not initially executed from loadBalancer
		lbObj.setNetworkEndTime(new Long(networkEndTime));
		new Thread(new CollectSystemData(lbObj),lbObj.getInstance()).start();

	}

	private int wrapperOK(CharResponseWrapper wrapper) {
		if (wrapper.getContentType() == null) return -1;
		if (wrapper.getContentType().indexOf("text/html") == -1) return -1;
		
		int ndx = wrapper.toString().indexOf("</head>");
		if (ndx == -1) ndx = wrapper.toString().indexOf("</form>");
		if (ndx == -1) ndx = wrapper.toString().indexOf("</body>");
		if (ndx == -1) ndx = wrapper.toString().indexOf("</html>");
		return ndx;
	}
}