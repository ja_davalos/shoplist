package com.assoc.jad;

import java.util.EnumSet;

import javax.faces.webapp.FacesServlet;
import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.log4j.BasicConfigurator;
import org.ocpsoft.rewrite.servlet.RewriteFilter;
import org.primefaces.webapp.filter.FileUploadFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.assoc.jad.lbinstance.LoadBalancerFilter;
import com.assoc.jad.tools.Configuration;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan({"com.assoc.jad"})
@ServletComponentScan 
public class ShoplistApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		BasicConfigurator.configure();
		
		SpringApplication.run(ShoplistApplication.class, args);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ShoplistApplication.class);
	}

    @Bean
    public Configuration Configuration() {
       return new Configuration();
    }
    @Bean
    public ServletRegistrationBean<FacesServlet> servletRegistrationBean() {
        FacesServlet servlet = new FacesServlet();
        return new ServletRegistrationBean<FacesServlet>(servlet, "*.xhtml");
    }

    @Bean
    public FilterRegistrationBean<RewriteFilter> rewriteFilter() {
        FilterRegistrationBean<RewriteFilter> rwFilter = new FilterRegistrationBean<RewriteFilter>(new RewriteFilter());
        rwFilter.setDispatcherTypes(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST,
                DispatcherType.ASYNC, DispatcherType.ERROR));
        rwFilter.addUrlPatterns("/*");
        return rwFilter;
    }
    @Bean
    public FilterRegistrationBean<FileUploadFilter> someFilterRegistration() {

        FilterRegistrationBean<FileUploadFilter> prime = new FilterRegistrationBean<FileUploadFilter>(new FileUploadFilter());
        prime.setDispatcherTypes(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST,
                DispatcherType.ASYNC, DispatcherType.ERROR));
        prime.addUrlPatterns("/*");
        return prime;
    }
    @Bean
    public FilterRegistrationBean<LoadBalancerFilter> loadBalancerFilterRegistration() {

        FilterRegistrationBean<LoadBalancerFilter> register = new FilterRegistrationBean<LoadBalancerFilter>(new LoadBalancerFilter());
        register.setDispatcherTypes(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST,
                DispatcherType.ASYNC, DispatcherType.ERROR));
        register.addUrlPatterns("/*");
        register.addInitParameter("script", System.getenv("LBHANDSHAKE"));
        return register;
    }
    @Bean
    public ServletContextInitializer initializer() {
        return new ServletContextInitializer() {
            @Override
            public void onStartup(ServletContext servletContext)
                    throws ServletException {
                //servletContext.setInitParameter("primefaces.THEME", "bluesky");
                servletContext.setInitParameter("javax.faces.FACELETS_SKIP_COMMENTS", "true");
                servletContext.setInitParameter("com.sun.faces.expressionFactory","com.sun.el.ExpressionFactoryImpl");
                servletContext.setInitParameter("primefaces.UPLOADER","commons");
            }
        };
    }
}
