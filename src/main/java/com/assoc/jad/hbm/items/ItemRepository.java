package com.assoc.jad.hbm.items;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository  extends JpaRepository<Items,Long>{

	List<Items> findAllByFamilyidAndStoreidAndNameIgnoreCase(Long familyid, Long storeid, String name);

	void deleteByFamilyidAndStoreidAndName(Long familyid, Long storeid, String name);

	List<Items> findAllByFamilyidAndStoreid(long familyid, long id);

	List<Items> findAllByFamilyidAndPhotoid(long familyid, Long photoid);

	List<Items> findAllByPhotoid(long photoid);

}
