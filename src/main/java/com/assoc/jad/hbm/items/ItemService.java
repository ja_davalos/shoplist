package com.assoc.jad.hbm.items;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemService {
	
	@Autowired
	private ItemRepository itemRepository;

	public Optional<Items> findAllById(long id) {
		return itemRepository.findById(id);
	}
	public void insertItem(Items item) {
		itemRepository.save(item);
	}
	public void updateItem(Items item) {
		itemRepository.save(item);
	}
	public List<Items> findAllByFamilyidAndStoreidAndNameIgnoreCase(Long familyid, Long storeid,String name) {
		return itemRepository.findAllByFamilyidAndStoreidAndNameIgnoreCase(familyid,storeid,name);
	}
	public void deleteByFamilyidAndStoreidAndName(Long familyid,Long storeid, String name) {
		itemRepository.deleteByFamilyidAndStoreidAndName(familyid,storeid,name);
	}
	public List<Items> findAllByFamilyidAndStoreid(long familyid, long id) {
		return itemRepository.findAllByFamilyidAndStoreid(familyid, id);
	}
	public List<Items> findAllByFamilyidAndPhotoid(long familyid, Long photoid) {
		return itemRepository.findAllByFamilyidAndPhotoid(familyid, photoid);
	}
	public List<Items> findAllByPhotoid(long photoid) {
		return itemRepository.findAllByPhotoid(photoid);
	}
}
