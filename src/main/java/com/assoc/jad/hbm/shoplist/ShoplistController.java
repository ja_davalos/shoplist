package com.assoc.jad.hbm.shoplist;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.assoc.jad.tools.ShopListStatic;

@Controller
public class ShoplistController {
	
	@Autowired
	private ShoplistService shoplistService;
	
	private Shoplist	shoplist;
	private String 	infomsg		="";
	private boolean SQLStatusOK = false;
	private String 	familyKey;
	
	public ShoplistController(Shoplist shoplist) {
		this.shoplist = shoplist;
	}
	public ShoplistController() {
		shoplist = new Shoplist();
	}
	private boolean shoplistExist() {
		String listname = ShopListStatic.specialChars(shoplist.getListname());

	   List<Shoplist> wrkList = shoplistService.findAllByListnameAndEmail(listname,shoplist.getEmail());
	   if (wrkList == null || wrkList.size() == 0) return false;
		
		if ( wrkList.size() > 1) infomsg = "and identical shoplist already exist ";
		shoplist = (Shoplist)wrkList.get(0);   //TODO there might be more than one row.
		familyKey += shoplist.getId();
		return true;
	}
	private Shoplist familyViaId(Long id) {
		SQLStatusOK=false;
		infomsg = "";
		
		List<Shoplist> wrkList = shoplistService.findAllById(id); 
		if (wrkList == null || wrkList.size() == 0) {
			infomsg = "there are no shoplist for id="+id;
			return null;
		}
		SQLStatusOK=true;
		return (Shoplist)wrkList.get(0);		
	}
	private List<Shoplist> familyWithEmail(String email) {
		SQLStatusOK=false;
		infomsg = "";
		List<Shoplist> wrkList = shoplistService.findAllByEmailIgnoreCase(email);
		if (wrkList == null || wrkList.size() == 0) {
			infomsg = "there are no shoplist tree for email="+email;
			return null;
		}
		SQLStatusOK=true;
		return wrkList;
	}
	private boolean insertShoplist() {
		Date date = new Date();
		shoplist.setCreatedate(new Timestamp(date.getTime()));
		shoplist.setListname(ShopListStatic.specialChars(shoplist.getListname()));

		shoplistService.insertShoplist(shoplist);
		familyKey = shoplist.getListname()+"."+shoplist.getId();
		setInfomsg("successfully added a new shoplist root: "+ShopListStatic.undoSpecialChars(familyKey));
		return true;
	}
	public void createFamily() {
		if (shoplistExist()) return;
		insertShoplist();
	}
	public void updateEmails() {
		if (shoplistExist()) return;
		insertShoplist();
		
	}
	/*
	 * getters and setters
	 */
	public String getInfomsg() {
		return infomsg;
	}
	public void setInfomsg(String infomsg) {
		this.infomsg = infomsg;
	}
	public Shoplist getFamily() {
		return shoplist;
	}
	public Shoplist getFamily(Long id) {
		return familyViaId(id);
	}
	public void setFamily(Shoplist shoplist) {
		this.shoplist = shoplist;
	}
	public boolean isSQLStatusOK() {
		return SQLStatusOK;
	}
	public String getFamilyKey() {
		return familyKey;
	}
	public void setFamilyKey(String familyKey) {
		this.familyKey = familyKey;
	}
	public List<Shoplist> getfamilyWithEmail(String email) {
		return familyWithEmail(email);
	}
}
