package com.assoc.jad.hbm.shoplist;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ShoplistRepository extends JpaRepository<Shoplist,Long> {
	
	public Shoplist findByEmail(String email);
	
	
	public void deleteByEmailIgnoreCaseContains(String parm);
	
    public List<Shoplist> findAllByListnameAndEmail(String title, String email);
    public List<Shoplist> findAllById(Long id);
	public List<Shoplist> findByEmailIgnoreCaseContaining(String parm);

	public List<Shoplist> findByEmailIgnoreCase(String email);
}
