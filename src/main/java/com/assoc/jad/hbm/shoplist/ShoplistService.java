package com.assoc.jad.hbm.shoplist;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShoplistService {
	
	@Autowired
	private ShoplistRepository shoplistRepository;

	public List<Shoplist> findAllByListnameAndEmail(String listname, String email) {
		return shoplistRepository.findAllByListnameAndEmail(listname, email);
	}
	public List<Shoplist> findAllById(long id) {
		return shoplistRepository.findAllById(id);
	}
	public List<Shoplist> findAllByEmailIgnoreCase(String email) {
		return shoplistRepository.findByEmailIgnoreCase(email);
	}
	public void insertShoplist(Shoplist shoplist) {
		shoplistRepository.save(shoplist);
	}
}
