package com.assoc.jad.hbm.stores;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.assoc.jad.hbm.items.ItemService;
import com.assoc.jad.hbm.items.Items;
import com.assoc.jad.hbm.photos.ItemPhotoService;
import com.assoc.jad.hbm.shoplist.Shoplist;
import com.assoc.jad.hbm.shoplist.ShoplistService;
import com.assoc.jad.tools.ShopListStatic;

@Controller
public class StoreController {
	@Autowired
	ShoplistService shoplistService;
	@Autowired
	StoreService storeService;
	@Autowired
	ItemService itemService;
	@Autowired
	ItemPhotoService itemPhotoService;
	
	private Shoplist	shoplist;
	private String 	infomsg		="";
	private boolean SQLStatus = false;
	private boolean familyinDB = false;
	private Stores store = new Stores();
	private Items items = new Items();
	private HashMap<String, HashMap<String,Object>> storeShopList = new HashMap<String,HashMap<String,Object>>();

	public StoreController(Shoplist shoplist) {
		this.shoplist = shoplist;
		familyinDB = familyExist();
		storeShopList = new HashMap<String,HashMap<String,Object>>();
	}
	@SuppressWarnings("unused")
	private StoreController() {
	}

	private boolean familyExist() {
		String listname = "";
		infomsg = "shoplist has not been initialized. Please enter your email";
		if (shoplist == null || shoplist.getListname() == null ) return false;
		
		if (shoplist.getListname() != null )
			listname = ShopListStatic.specialChars(shoplist.getListname());
	
		List<Shoplist> wrkList = shoplistService.findAllByListnameAndEmail(listname,shoplist.getEmail());
		infomsg = "shoplist does not exit for shoplist id="+shoplist.getId();
		if (wrkList == null || wrkList.size() == 0) return false;
		
		infomsg = "";
		return true;
	}
	private boolean addStore(String name) {
		infomsg = "failed to add to store row to table-stores="+name+" for shoplist= "+ shoplist.getListname();
		if (name.trim().length() == 0) return false;
		
		name  = ShopListStatic.specialChars(name);

		Date date = new Date();
		store.setCreatedate(new Timestamp(date.getTime()));
		store.setFamilyid(shoplist.getId());
		store.setName(name);

		List<Stores> wrkList = storeService.findAllByFamilyidAndNameIgnoreCase(shoplist.getId(), name); 
		infomsg = "shoplist does not exit for shoplist id="+shoplist.getId();
		if (wrkList == null || wrkList.size() == 0) {
			storeService.insertStore(store);
		} else store = wrkList.get(0);
		infomsg = "";
		return true;
	}
	private boolean itemExist(String name) {
		name  = ShopListStatic.specialChars(name);

		List<Items> wrkList = itemService.findAllByFamilyidAndStoreidAndNameIgnoreCase(shoplist.getId(), store.getId(), name); 
		if (wrkList == null || wrkList.size() == 0) return false;
		
		infomsg = "identical item="+name+"already exist for shopping list for store="+store.getId();
		System.out.println(infomsg);
		return true;
	}
	private boolean insertItem(String name) {
		SQLStatus=false;

		Date date = new Date();
		items.setCreatedate(new Timestamp(date.getTime()));
		items.setName(ShopListStatic.specialChars(name));
		items.setFamilyid(shoplist.getId());
		items.setStoreid(store.getId());
		items.setPhotoid(-1);

		itemService.insertItem(items);
		return true;
	}
	private boolean deleteItem(String name) {
		name = ShopListStatic.specialChars(name);
		itemService.deleteByFamilyidAndStoreidAndName(shoplist.getId(), store.getId(), name);
		return true;
	}
	private boolean updateItem() {
		itemService.updateItem(items);
		return true;
	}
	private boolean addItems(List<String> list) {
		String previous = "";
		for (int i=0;i<list.size();i++) {
			if (previous.equals(list.get(i)) || list.get(i).trim().length() == 0) continue;
			previous = list.get(i).trim();
			if(itemExist(list.get(i))) continue;
			
			if(!insertItem(list.get(i))) return false;
		}
		return true;
	}
	private boolean deleteItems(List<String> list) {
		for (int i=0;i<list.size();i++) {
			if (list.get(i).trim().length() == 0) continue;
			
			if(!deleteItem(list.get(i))) return false;
		}
		return true;
	}
	private boolean resyncItemPhoto(HashMap<String, Long> photoSyncHash) {
		infomsg = "";
		for (String name: photoSyncHash.keySet()) {
			Long photoid = (Long)photoSyncHash.get(name);
			if (photoid == null || photoid == -1) continue;
			
			List<Items> itemList = itemService.findAllByFamilyidAndPhotoid(shoplist.getId(), photoid);
			if (itemList != null && itemList.size() > 0) continue;
			
			itemPhotoService.deleteByFamilyidAndId(shoplist.getId(),photoid);
		}
		return true;
	}
	private List<Items> getAllItemsFromStore(long id) {
		if (!familyinDB) return null;		
		return itemService.findAllByFamilyidAndStoreid(shoplist.getId(), id); 
	}
	private void setAllStores() {
		setSQLStatusOK(false);
		infomsg = "";
		List<Stores> storeList = storeService.findAllByFamilyid(shoplist.getId()); 
		if (storeList == null || storeList.size() == 0) {
			infomsg = "there are no stores for this 'shoplist' id="+shoplist.getId()+" name="+shoplist.getListname();
			return;
		}
		for (int i=0;i<storeList.size();i++) {
			store = (Stores)storeList.get(i);
			List<Items> itemList = this.getAllItemsFromStore(store.getId());
			if (itemList == null || storeList.size() == 0) continue;
			HashMap<String,Object> wrkHashMap = new HashMap<String,Object>();
			for (int j=0;j<itemList.size();j++) {
				Items wrkItem = (Items)itemList.get(j);
				wrkHashMap.put(wrkItem.getName(), wrkItem);
			}
			if (wrkHashMap.size() > 0)
				storeShopList.put(ShopListStatic.undoSpecialChars(store.getName()), wrkHashMap);
		}
		setSQLStatusOK(true);
	}
	private List<Items> getStoreItemsUsingStoreName(String storename) {
		if (storename == null || storename.length() == 0) return null;
		
		storename  = ShopListStatic.specialChars(storename);

		List<Stores> wrkList = storeService.findAllByFamilyidAndNameIgnoreCase(shoplist.getId(), storename);
		
		infomsg = "shoplist does not exit for shoplist id="+shoplist.getId();
		if (wrkList == null || wrkList.size() == 0) return null;
		store = wrkList.get(0);
		return getAllItemsFromStore(store.getId());
	}
	
	public List<Items> getStoreItems(long id) {
		return getAllItemsFromStore(id);
	}
	public void addList(String storeName,List<String> list) {
		if (!familyinDB) return;
		if (!addStore(storeName)) return;
		if (!addItems(list)) return;
		infomsg = "success adding item list to store="+store.getName()+" for shoplist= "+ shoplist.getListname();

	}
	public void deleteList(String storeName,List<String> list) {
		if (!familyinDB) return;
		if (!addStore(storeName)) return;
		if (!deleteItems(list)) return;
		infomsg = "success deleted items from list to store="+store.getName()+" for shoplist= "+ shoplist.getListname();
	}
	public void updateList(String storeName,List<String> list) {
		if (!familyinDB) return;
		if (!addStore(storeName)) return;
		if (!addItems(list)) return;
		infomsg = "success update items from list to store="+store.getName()+" for shoplist= "+ shoplist.getListname();
	}
	public boolean isSQLStatusOK() {
		return SQLStatus;
	}
	public void loadAllStores() {	
		if (!familyinDB) return;
		setAllStores();
	}
	public boolean updateStoreItem(Items item) {
		this.items = item;
		this.store = new Stores();
		store.setId(item.getStoreid());
		return updateItem();
	}

	public void updateStoreItemsJson(String key, JSONArray  jsonItems) {
		List<Items> itemList = getStoreItemsUsingStoreName( key);
		if (itemList == null || itemList.size() == 0) return;

		for (int i=0;i<jsonItems.size();i++) {
			JSONObject jsonItem = (JSONObject) jsonItems.get(i);
			String desc = (String)jsonItem.get("desc");
			String name = (String)jsonItem.get("name");
			Boolean selected = (Boolean)jsonItem.get("selected");
			
			for (int j=0;j<itemList.size();j++) {
				items = (Items)itemList.get(j);
				if (!items.getName().equals(name)) continue;
				
				items.setDescriptions(desc);
				items.setBuyingorder(((Long)jsonItem.get("buyingorder")).intValue());
				items.setActive(0);
				if (selected) items.setActive(1);
				updateItem();
				break;
			}
		}
	}
	public void addDeleteTempItems(String key, JSONArray jsonObjs) {

		for (int i=0;i<jsonObjs.size();i++) {
			JSONObject jsonItem = (JSONObject) jsonObjs.get(i);
			Boolean temporary = (Boolean)jsonItem.get("temporary");
			String name = (String)jsonItem.get("name");
			Boolean selected = (Boolean)jsonItem.get("selected");
			if (!temporary) continue;
			
			items.setTemp(temporary);
			if(selected ) {
				if(itemExist(name)) continue;
				if(!insertItem(name)) return;
			} else {
				if (!deleteItem(name)) return;
			}
		}
		
	}
	public void resynchItemPhoto(HashMap<String, Long> photoSyncHash) {
		resyncItemPhoto(photoSyncHash);	
	}
	public boolean initStore(Shoplist shoplist) {
		this.shoplist = shoplist;
		familyinDB = familyExist();
		storeShopList = new HashMap<String,HashMap<String,Object>>();
		return familyinDB;
	}
/*
 * getters and setters
 */
	public void setSQLStatusOK(boolean sQLStatusOK) {
		SQLStatus = sQLStatusOK;
	}
	public String getInfomsg() {
		return infomsg;
	}
	public void setInfomsg(String infomsg) {
		this.infomsg = infomsg;
	}
	public HashMap<String, HashMap<String, Object>> getStoreShopList() {
		return storeShopList;
	}
	public void setStoreShopList(HashMap<String, HashMap<String, Object>> storeShopList) {
		loadAllStores();
		this.storeShopList = storeShopList;
	}
	public Shoplist getShoplist() {
		return shoplist;
	}
	public void setShoplist(Shoplist shoplist) {
		this.shoplist = shoplist;
	}
}
