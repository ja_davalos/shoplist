package com.assoc.jad.hbm.stores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreService {
	
	@Autowired
	private StoreRepository storeRepository;

	public List<Stores> findAllById(long id) {
		return storeRepository.findAllById(id);
	}
	public void insertStore(Stores store) {
		storeRepository.save(store);
	}
	public void updateStore(Stores store) {
		storeRepository.save(store);
	}
	public List<Stores> findAllByFamilyidAndNameIgnoreCase(Long familyid, String name) {
		return storeRepository.findAllByFamilyidAndNameIgnoreCase(familyid,name);
	}
	public List<Stores> findAllStores() {
		return storeRepository.findAll();
	}
	public List<Stores> findAllByFamilyid(long id) {
		return storeRepository.findAllByFamilyid( id);
	}
}
