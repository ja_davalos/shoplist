package com.assoc.jad.hbm.stores;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepository extends JpaRepository<Stores,Long> {
	
    public List<Stores> findAllById(Long id);
	public List<Stores> findAllByFamilyidAndNameIgnoreCase(Long familyid, String name);
	public List<Stores> findAllByFamilyid(long id);
}
