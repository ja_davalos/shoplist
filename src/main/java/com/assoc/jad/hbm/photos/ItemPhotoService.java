package com.assoc.jad.hbm.photos;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemPhotoService {
	
	@Autowired
	private ItemPhotoRepository itemPhotoRepository;

	public Optional<ItemPhotos> findAllById(long id) {
		return itemPhotoRepository.findById(id);
	}
	public void insertItemPhoto(ItemPhotos photo) {
		itemPhotoRepository.save(photo);
	}
	public void updateItemPhoto(ItemPhotos photo) {
		itemPhotoRepository.save(photo);
	}
	public void deleteItemPhoto(Long id) {
		itemPhotoRepository.deleteById(id);
	}
	public void deleteByFamilyidAndId(long familyid, Long photoid) {
		itemPhotoRepository.deleteByFamilyidAndId(familyid,photoid);
		
	}
	public List<ItemPhotos> findAllByFamilyidAndId(Long familyid, Long id) {
		// TODO Auto-generated method stub
		return itemPhotoRepository.findAllByFamilyidAndId(familyid,id);
	}
	public void deleteByFamilyidAndName(long familyid, String name) {
		itemPhotoRepository.deleteByFamilyidAndName(familyid,name);		
	}
}
