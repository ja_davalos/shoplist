package com.assoc.jad.hbm.photos;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemPhotoRepository  extends JpaRepository<ItemPhotos,Long>{

	void deleteByFamilyidAndId(long familyid, Long photoid);

	List<ItemPhotos> findAllByFamilyidAndId(Long familyid, Long id);

	void deleteByFamilyidAndName(long familyid, String name);

}
