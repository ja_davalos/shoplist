package com.assoc.jad.hbm.family;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This class represents a row in families table in our application's  model.
 * 
 * @author jad
 * 
 */
@Entity
public class Family implements Comparable<Family>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private long id;
	private long familyid;
	private Timestamp createdate;
	private String email;
	private String firstname;
	
	public Family() {
	}

	public int compareTo(Family o) {
		return this.email.compareTo(o.email); //&& this.lastname2.compareTo(o.lastname2);
	}
	@Override
	public String toString() {
		return id+email;
	}
	public void setCreatedate(Timestamp createdate) {
		this.createdate = createdate;
	}
	public Timestamp getCreatedate() {
		return createdate;
	}

	public void setId(Long id) {
		this.id = id.longValue();
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public long getId() {
		return id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public long getFamilyid() {
		return familyid;
	}

	public void setFamilyid(long familyid) {
		this.familyid = familyid;
	}
}
