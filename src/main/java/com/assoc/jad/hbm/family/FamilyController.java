package com.assoc.jad.hbm.family;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.assoc.jad.tools.ShopListStatic;

@Controller 
public class FamilyController {
	
	@Autowired
	FamilyService familyService;
	
	private Family	family;
	private String 	infomsg		="";
	private boolean SQLStatusOK = false;
	private String 	familyKey;
	
	public FamilyController(Family family) {
		this.family = family;
	}
	public FamilyController() {
		family = new Family();
	}
	private boolean familyExist() {
		SQLStatusOK=false;
		String Firstname = ShopListStatic.specialChars(family.getFirstname());


		List<Family> wrkList = familyService.findAllByFirstnameAndEmail(Firstname,family.getEmail());
		if (wrkList == null || wrkList.size() == 0) return false;
		
		infomsg = "and identical family already exist ";
		family = (Family)wrkList.get(0);   //TODO there might be more than one row.
		familyKey += family.getId();
		return true;
	}
	private Family familyViaId(Long id) {
		SQLStatusOK=false;
		infomsg = "";
		Optional<Family> wrkList = familyService.findById(id);
		if (!wrkList.isPresent()) {
			infomsg = "there are no family for id="+id;
			return null;
		}
		SQLStatusOK=true;
		return wrkList.get();
	}
	private List<Family> familyWithEmail(String email) {
		SQLStatusOK=false;
		infomsg = "";
		List<Family> wrkList = familyService.findAllByEmailIgnoreCase(email); 
		if (wrkList == null || wrkList.size() == 0) {
			infomsg = "there are no family tree for email="+email;
			return null;
		}
		SQLStatusOK=true;
		return wrkList;
	}
	private boolean insertFamily() {

		Date date = new Date();
		family.setCreatedate(new Timestamp(date.getTime()));
		family.setFirstname(ShopListStatic.specialChars(family.getFirstname()));

		familyService.insert(family);
		setInfomsg("successfully added a new family root: "+ShopListStatic.undoSpecialChars(familyKey));
		return true;
	}
	public void createFamily() {
		if (familyExist()) return;
		insertFamily();
	}
	public void updateEmails() {
		if (familyExist()) return;
		insertFamily();
		
	}
	/*
	 * getters and setters
	 */
	public String getInfomsg() {
		return infomsg;
	}
	public void setInfomsg(String infomsg) {
		this.infomsg = infomsg;
	}
	public Family getFamily() {
		return family;
	}
	public Family getFamily(Long id) {
		return familyViaId(id);
	}
	public void setFamily(Family family) {
		this.family = family;
	}
	public boolean isSQLStatusOK() {
		return SQLStatusOK;
	}
	public String getFamilyKey() {
		return familyKey;
	}
	public void setFamilyKey(String familyKey) {
		this.familyKey = familyKey;
	}
	public List<Family> getfamilyWithEmail(String email) {
		return familyWithEmail(email);
	}
}
