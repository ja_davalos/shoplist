package com.assoc.jad.hbm.family;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FamilyService {
	@Autowired 
	FamilyRepository familyRepository;

	public List<Family> findAllByFirstnameAndEmail( String firstname, String email) {
		return familyRepository.findAllByFirstnameAndEmail(firstname, email);
	}

	public Optional<Family> findById(Long id) {
		return familyRepository.findById(id);
	}

	public List<Family> findAllByEmailIgnoreCase(String email) {
		return familyRepository.findAllByEmailIgnoreCase(email);
	}

	public void insert(Family family) {
		familyRepository.save(family);	
	}

}
