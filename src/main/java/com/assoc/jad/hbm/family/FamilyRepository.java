package com.assoc.jad.hbm.family;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FamilyRepository extends JpaRepository<Family,Long> {

	List<Family> findAllByFirstnameAndEmail(String firstname, String email);

	List<Family> findAllByEmailIgnoreCase(String email);

}
