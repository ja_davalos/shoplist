package com.assoc.jad.hbm.email;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.assoc.jad.hbm.shoplist.Shoplist;
import com.assoc.jad.hbm.shoplist.ShoplistService;
import com.assoc.jad.tools.ShopListStatic;

@Controller
public class EmailController {
	
	@Autowired
	EmailService emailService;
	@Autowired
	ShoplistService shoplistService;
	
	private Shoplist	shoplist;
	private String 	infomsg		="";
	private boolean SQLStatusOK = false;
	private String 	shoplistKey;
	private Emails emails = new Emails();

	public EmailController(Shoplist shoplist) {
		this.shoplist = shoplist;
	}
	@SuppressWarnings("unused")
	private EmailController() {
		shoplist = null;
	}
	private boolean familyExist() {
		SQLStatusOK=false;
		if (shoplist == null) return false;
		
		String listname  = ShopListStatic.specialChars(shoplist.getListname());

		shoplistKey = shoplist.getListname()+".";

		List<Shoplist> wrkList = shoplistService.findAllByListnameAndEmail(listname, shoplist.getEmail()); 
		if (wrkList == null || wrkList.size() == 0) return false;
		
		infomsg = "OK shoplist exist ";
		shoplist = wrkList.get(0);
		shoplistKey += shoplist.getId();
		SQLStatusOK=true;
		return true;
	}
	private List<Shoplist> familyWithEmail(String email) {
		SQLStatusOK=false;
		infomsg = "";
		List<Shoplist> wrkList = shoplistService.findAllByEmailIgnoreCase(email); 
		if (wrkList == null || wrkList.size() == 0) {
			infomsg = "there are no shoplist tree for email="+email;
		}
		return wrkList;
	}
	private boolean emailExist(String email) {
		List<Emails> wrkList = emailService.findAllByFamilyidAndEmailIgnoreCase(shoplist.getId(),email); 
		if (wrkList == null || wrkList.size() == 0) return false;
		
		infomsg = "and identical email already exist for shopping list for shoplist="+shoplist.getId();
		System.out.println(infomsg);
		return true;
	}
	private boolean insertEmail(String email) {
		SQLStatusOK=false;

		Date date = new Date();
		emails.setCreatedate(new Timestamp(date.getTime()));
		emails.setEmail(email);
		emails.setFamilyid(shoplist.getId());

		emailService.insert(emails);
		shoplistKey = shoplist.getListname()+"."+shoplist.getId();
		setInfomsg("successfully added an email record to shoplist: "+ShopListStatic.undoSpecialChars(shoplistKey));
		return true;
	}
	public void addEmails(String emailList) {
		if (!familyExist()) return;
		infomsg = "";
		
		String[] allEmails = emailList.split(System.lineSeparator());
		String previous = "";
		for (int i=0;i<allEmails.length;i++) {
			if (previous.equals(allEmails[i]) || allEmails[i].trim().length() == 0) continue;
			previous = allEmails[i].trim();
			if(emailExist(allEmails[i])) continue;
			
			if(!insertEmail(allEmails[i])) return;
		}
		
	}
	public void initEmail(Shoplist shoplist) {
		this.shoplist = shoplist;

	}
	/*
	 * getters and setters
	 */
	public String getInfomsg() {
		return infomsg;
	}
	public void setInfomsg(String infomsg) {
		this.infomsg = infomsg;
	}
	public Shoplist getShoplist() {
		return shoplist;
	}
	public void setShoplist(Shoplist shoplist) {
		this.shoplist = shoplist;
	}
	public boolean isSQLStatusOK() {
		return SQLStatusOK;
	}
	public String getShoplistKey() {
		return shoplistKey;
	}
	public void setShoplistKey(String shoplistKey) {
		this.shoplistKey = shoplistKey;
	}
	public List<Shoplist> getfamilyWithEmail(String email) {
		return familyWithEmail(email);
	}
}
