package com.assoc.jad.hbm.email;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
	@Autowired
	private EmailRepository emailRepository;

	public void insert(Emails email) {
		emailRepository.save(email);
	}

	public List<Emails> findAllByFamilyidAndEmailIgnoreCase(long familyid, String email) {
		// TODO Auto-generated method stub
		return emailRepository.findAllByFamilyidAndEmailIgnoreCase(familyid, email);
	}

}
