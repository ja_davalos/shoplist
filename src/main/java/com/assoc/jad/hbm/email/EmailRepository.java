package com.assoc.jad.hbm.email;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepository extends JpaRepository<Emails,Long>{

	List<Emails> findAllByFamilyidAndEmailIgnoreCase(long familyid, String email);

}
