package com.assoc.jad.hbm;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.assoc.jad.tools.ShopListStatic;

public class hibernateDBAccess {
	private static final char DELETE = 'D';
	private static final char INSERT = 'I';
	private static final char UPDATE = 'U';
	
	private Object instance = null;
	private String errorMsg;

	public hibernateDBAccess(Object instance) {
		this.instance = instance;
	}
	private<T> boolean hibernateCmd(T objGen,char oper) {
		
        boolean flag= false;
        Session session = ShopListStatic.getFactory().openSession();
		Transaction tx = null;
		try {
		   tx = session.beginTransaction();
			switch (oper) {
			case DELETE:
		        session.delete(objGen);
		        flag= true;
				break;
			case UPDATE:
		        session.update(objGen);
		        flag= true;
				break;
			case INSERT:
		        session.save(objGen);
		        flag= true;
				break;
			default:
				break;
			}
		   tx.commit();
		} catch (Exception e) {
		   if (tx!=null) tx.rollback();
		   e.printStackTrace(); 
		   flag = false;
		} finally {
		   session.close();
		}
		return flag;
	}

	/**
     * executes sql using Hibernate 
     */
	public List<?> readSql( String sql) {
        Session session = ShopListStatic.getFactory().openSession();
		@SuppressWarnings("deprecation")
		List<?> wrkList = session.createSQLQuery(sql).addEntity(this.instance.getClass()).list();
		session.close();
		return wrkList;
	}
	public<T> boolean insertSql(T objGen) {
		return  hibernateCmd(objGen,INSERT);
	}
	public boolean updateSql(Object objGen) {
		return  hibernateCmd(objGen,UPDATE);
	}
	public boolean deleteSql(Object objGen) {
		return  hibernateCmd(objGen,DELETE);
	}

/*
 * getters and setters 
 */
	public String getErrorMsg() {
		return errorMsg;
	}
}