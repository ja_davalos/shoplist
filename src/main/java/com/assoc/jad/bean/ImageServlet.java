package com.assoc.jad.bean;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.assoc.jad.hbm.hibernateDBAccess;
import com.assoc.jad.hbm.items.Items;
import com.assoc.jad.hbm.photos.ItemPhotos;

@WebServlet(urlPatterns = "/image/*", loadOnStartup = 1, asyncSupported = true)

public class ImageServlet extends HttpServlet {
	
	//@Autowired spring boot doesn't control the life cycle for webservlets therefore no Injections
	//private ItemPhotoService  itemPhotoService = new ItemPhotoService();
	//private ItemService  itemService = new ItemService();
	
	private static final long serialVersionUID = 1L;
	private static final String HTMLWrapper = "<html><body>%s </body></html>";
	private String infomsg;

	public void init(ServletConfig config) {
		System.out.println("ImageServlet has been initialized");
	}

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		
		JSONObject jsonObj;
		String jsonitem = request.getParameter("jsonitem");
		if (jsonitem == null)
			return;
		try {
			jsonObj = (JSONObject) new JSONParser().parse(jsonitem);
		} catch (ParseException e) {
			throw new RuntimeException("Unable to parse json " + jsonitem);
		}
		ItemPhotos itemPhoto = new ItemPhotos();
		Long familyid = (Long)jsonObj.get("familyid");
		Long photoid  = (Long)jsonObj.get("photoid");
		
		hibernateDBAccess hibDBAcc = new hibernateDBAccess(itemPhoto);
		String sql = "select * from itemphotos where familyid="+familyid+ " and id="+photoid;
		List<?> wrkObjs = (List<?>) hibDBAcc.readSql(sql);

		if (wrkObjs.size() == 0) {
			infomsg = "file for "+jsonObj.get("name")+" not found familyid="+familyid+" photoid="+photoid;
			infomsg = String.format(HTMLWrapper, infomsg);
			response.getOutputStream().write(infomsg.getBytes());
			resetItemPhotoPointer(jsonObj);
			return;
		}
		itemPhoto = (ItemPhotos)wrkObjs.get(0);
		String ext = "jpg";
		int ndx = itemPhoto.getName().lastIndexOf('.');
		if (ndx != -1) ext = itemPhoto.getName().substring(++ndx);
		response.setContentType("image/"+ext);
		response.setContentLength(itemPhoto.getPhoto().length);
		response.getOutputStream().write(itemPhoto.getPhoto());
	}

	private void resetItemPhotoPointer(JSONObject jsonObj) {
		Items item = new Items();
		Long familyid = new Long((String)jsonObj.get("familyid"));
		Long photoid  = new Long((String)jsonObj.get("photoid"));
		
		hibernateDBAccess hibDBAcc = new hibernateDBAccess(item);
		String sql = "select * from items where familyid="+familyid+ "id="+photoid;
		List<?> wrkObjs = (List<?>) hibDBAcc.readSql(sql);

		for (int i=0;i<wrkObjs.size();i++) {
			item = (Items)wrkObjs.get(i);
			item.setPhotoid(-1);
			hibDBAcc.updateSql(item);
		}
	}

	public String getInfomsg() {
		return infomsg;
	}

	public void setInfomsg(String infomsg) {
		this.infomsg = infomsg;
	}
}
