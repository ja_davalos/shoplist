package com.assoc.jad.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.assoc.jad.hbm.email.EmailController;
import com.assoc.jad.hbm.shoplist.Shoplist;
import com.assoc.jad.hbm.shoplist.ShoplistController;
import com.assoc.jad.hbm.stores.StoreController;
import com.assoc.jad.tools.ShopListStatic;

@Scope(value = "session")
@Component(value = "familyBean")
@ELBeanName(value = "familyBean")
public class FamilyBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Autowired
	ShoplistController shoplistController;
	@Autowired
	StoreController storeController;
	@Autowired
	EmailController emailController;

	private static final String REDIRECTED = "redirected";
	private Shoplist	shoplist 		= new Shoplist();
	private String 	infomsg="";
	private String 	familyKey;
	private String	emailList;
	private String	itemList;
	private String	navigator;
	private String email;
	private String storeName;
	private List<SelectItem> familyList  = new ArrayList<SelectItem>();
	private String familyId;

	public FamilyBean() {
	}
	private void addFamilyItem(String name,Long id) {
		SelectItem item = new SelectItem();
		item.setLabel(name+";"+email);
		item.setValue(id.toString());
		familyList.add(item);		
	}
	private String getHTMLParam(String name) {
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext external = context.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) external.getRequest();
		return request.getParameter(name);
	}
	private void resetCookie(String name) {
		HttpServletRequest req   =  (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpServletResponse resp =  (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
		Cookie[] cookies = req.getCookies();
		if (cookies != null) 
			for (int i=0;i<cookies.length;i++) {
				if (name.equals(cookies[i].getName())) {
					cookies[i].setValue("");
		            cookies[i].setMaxAge(0);
		            resp.addCookie(cookies[i]);
					break;
				}
			}
	}
	private void getEmailFromCookie() {
		String wrkEmail = "";
		HttpServletRequest req   =  (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		Cookie[] cookies = req.getCookies();
		if (cookies != null) 
			for (int i=0;i<cookies.length;i++) {
				if ("email".equals(cookies[i].getName())) {
					wrkEmail = cookies[i].getValue();
					break;
				}
			}
		if (wrkEmail.length() == 0 || wrkEmail.equals(email)) {
			wrkEmail = getHTMLParam("email");
			if (wrkEmail == null) wrkEmail = getHTMLParam("parm"); //iphone interprets 'email' to start mail app
			if (wrkEmail != null && (wrkEmail.length() == 0 || wrkEmail.equals(email))) return;
		}
		if (wrkEmail != null) email = wrkEmail;
		getFamilyUsingEmail();
	}
	private void setEmailFromCookie() {
		HttpServletResponse resp =  (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
		Cookie cookie = new Cookie("email",email);
		try {  //android fails when setting a cookie
	    resp.addCookie( cookie );
		}catch (Exception e) {}
	}
	private boolean isFamilyReady() {
		infomsg="";
		if (shoplist.getId() <= 0 || shoplist.getListname() == null) {
			infomsg = "needs to have a shoplist record. Click 'select a shoplist' from menu";
			//savedNavigation = getRedirect();
			return false;
		}
		getFamilyUsingId(); //resync shoplist object with possible changes to 'familyid'
		return true;
	}
	private boolean getMasterListBean() {
		ExternalContext external = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest req  = (HttpServletRequest) external.getRequest();
		MasterListBean masterListBean = (MasterListBean)req.getSession().getAttribute("masterListBean");
		if (masterListBean == null) {
			masterListBean = new MasterListBean();
			req.getSession().setAttribute("masterListBean",masterListBean);
		}
		storeController.initStore(shoplist);
		storeController.loadAllStores();
		masterListBean.setStoreShopList(storeController.getStoreShopList());
		masterListBean.setShoplist(shoplist);
		return true;
	}
	private void getFamilyUsingEmail() {
		if (email == null || email.length() == 0) return;
		if (shoplist != null && 
			(familyId != null && familyId.length() > 0) &&
			email.equals(shoplist.getEmail()) && shoplist.getId() == (new Long(familyId)).longValue()) return;
		
		List<Shoplist> wrkList = shoplistController.getfamilyWithEmail(email);
		if (wrkList == null) {
			ShopListStatic.redirect(ShopListStatic.NEWSHOPPINGLIST);
			return;
		}
		
		getMasterListBean();
		if (wrkList.size() == 1) {
			shoplist = (Shoplist)wrkList.get(0);
			familyId = new Long(shoplist.getId()).toString();
			ShopListStatic.redirect(ShopListStatic.STORELIST);
			return;
		}
		familyList  = new ArrayList<SelectItem>();
		for (int i=0;i<wrkList.size();i++) {
			shoplist = (Shoplist)wrkList.get(i);
			addFamilyItem(shoplist.getListname(),shoplist.getId());
		}
		familyId = "";
		navigator = FamilyBean.REDIRECTED;
		ShopListStatic.redirect(ShopListStatic.SELECTFAMILY2);
		return;
	}
	private void getFamilyUsingId() {
		if (familyId == null || familyId.length() == 0) return;
		if (shoplist.getId() == (new Long(familyId)).longValue()) {
			ShopListStatic.redirect(ShopListStatic.STORELIST);
			return;
		}
		
		shoplist = shoplistController.getFamily(new Long(familyId));
		getMasterListBean();
		ShopListStatic.redirect(ShopListStatic.STORELIST);
	}
	private boolean addFamily() {
		shoplistController.createFamily();
		infomsg = shoplistController.getInfomsg();
		familyKey = shoplistController.getFamilyKey();
		shoplist = shoplistController.getFamily();
		return shoplistController.isSQLStatusOK();
	}
	private void resetInfomsg() {
		HttpServletRequest req  = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		MasterListBean masterListBean = (MasterListBean)req.getSession().getAttribute("masterListBean");
		if (masterListBean != null) masterListBean.setInfomsg("");
		infomsg = "";
	}
	
	
	public void familyViaEmail(ActionEvent event) {
		if (REDIRECTED.equals(navigator)) return;
		navigator = "selectFamily";
		getFamilyUsingId();
	}
	public void newShoppingList(ActionEvent event) {
		shoplist.setEmail(email);
		addFamily();
	}
	public void additemList(ActionEvent event) {
		resetInfomsg();
		if (storeName == null || storeName.length() == 0 || 
			itemList == null || itemList.trim().length() == 0) return;
		
		String[] wrkArray = itemList.split("(:|\\s+|\\n|\\t|;|,)");
		List<String> list = Arrays.asList(wrkArray);;

		storeController.initStore(shoplist);
		storeController.loadAllStores();
		storeController.addList(storeName,list);
		infomsg = storeController.getInfomsg();
	}

	public void addFamily(ActionEvent event) {
			navigator = "addFamily";
			if (addFamily()) navigator = "addEmails";
	}
	public void addEmails(ActionEvent event) {
		navigator = "getShoplist";
		familyId = null;  //shoplist already verified familyid is not releveant
		if (!isFamilyReady()) return;
		
		emailController.initEmail(shoplist);
		emailController.addEmails(emailList);
		infomsg = emailController.getInfomsg();
		familyKey = emailController.getShoplistKey();
		shoplist = emailController.getShoplist();
		navigator = "addEmails";
		if (emailController.isSQLStatusOK()) navigator = "addShoppingList";
	}
	public String navigation() {
		return navigator;
	}
	/*
	 * getters and setters
	 */
	public Shoplist getShoplist() {
		return shoplist;
	}
	public void setFamily(Shoplist shoplist) {
		this.shoplist = shoplist;
	}
	public String getInfomsg() {
		return infomsg;
	}
	public void setInfomsg(String infomsg) {
		this.infomsg = infomsg;
	}
	public String getFamilyKey() {
		return ShopListStatic.undoSpecialChars(familyKey);
	}
	public void setFamilyKey(String familyKey) {
		this.familyKey = familyKey;
	}
	public void setEmail(String email) {
		setEmailFromCookie();
		this.email = email;
	}
	public String getEmail() {
		getEmailFromCookie();
		return email;
	}
	public String getEmail2() {
		return email;
	}
	public void setEmail2(String email) {
		this.email = email;
	}
	public String getEmailList() {
		return emailList;
	}
	public void setEmailList(String emailList) {
		this.emailList = emailList;
	}
	public List<SelectItem> getFamilyList() {
		if (email == null || email.length() == 0) return familyList;

		familyList = new ArrayList<SelectItem>();
		List<Shoplist> wrkList = shoplistController.getfamilyWithEmail(email);
		if (wrkList == null) {
			infomsg = "no shoplist for email="+email+" in DB";
			return familyList;
		}
		infomsg = shoplistController.getInfomsg();
		Shoplist wrkFamily = new Shoplist();
		for (int i=0;i<wrkList.size();i++) {
			wrkFamily = (Shoplist)wrkList.get(i);
			addFamilyItem(wrkFamily.getListname(),wrkFamily.getId());
		}
		return familyList;
	}
	public String getHtml() {
		if (navigator == null || navigator.length() == 0) navigator = "addFamily";
		return navigator;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getFamilyId() {
		getFamilyUsingId();
		return familyId;
	}
	public void setFamilyId(String familyId) {
		this.familyId = familyId;
		getFamilyUsingId();
	}
	public void setResetFamily(String familyId) {
		resetCookie("email");
		email = "";
		this.familyId = null;
	}
	public String getResetFamily() {
		this.familyId = null;
		resetCookie("email");
		familyList  = new ArrayList<SelectItem>();
		email = "";
		resetInfomsg();
		return "";
	}
	public String getItemList() {
		return itemList;
	}
	public void setItemList(String itemList) {
		this.itemList = itemList;
	}
}
