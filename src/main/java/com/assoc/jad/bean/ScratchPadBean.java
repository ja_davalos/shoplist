package com.assoc.jad.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class ScratchPadBean {
	private String saveSelectedList;

	public String getSaveSelectedList() {
		return saveSelectedList;
	}

	public void setSaveSelectedList(String saveSelectedList) {
		this.saveSelectedList = saveSelectedList;
	}
}
