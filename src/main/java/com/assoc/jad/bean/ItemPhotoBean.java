package com.assoc.jad.bean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.assoc.jad.hbm.items.ItemService;
import com.assoc.jad.hbm.items.Items;
import com.assoc.jad.hbm.photos.ItemPhotoService;
import com.assoc.jad.hbm.photos.ItemPhotos;

@ManagedBean@Scope(value = "session")
@Component(value = "itemPhotoBean")
@ELBeanName(value = "itemPhotoBean")
public class ItemPhotoBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Autowired
	ItemService itemService;
	@Autowired
	ItemPhotoService itemPhotoService;
	
	private String infomsg;

	public ItemPhotoBean() {
	}
	public ItemPhotos updatePhoto(UploadedFile uploadedFile,String name,long shoplistid ) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) return null;

		ItemPhotos photo = new ItemPhotos();
		itemPhotoService.deleteByFamilyidAndName(shoplistid,name);
		
		java.util.Date dateBin = new java.util.Date();
		photo.setName(name);
		photo.setCreatedate(new Timestamp(dateBin.getTime()));
		photo.setFamilyid(shoplistid);
		photo.setPhoto(uploadedFile.getContents());
		itemPhotoService.insertItemPhoto(photo);
		
		return photo;
	}
	public boolean resetItemsPhotoPointer(ItemPhotos itemPhoto) {
		Items item = new Items();

		List<Items> wrkList = itemService.findAllByPhotoid(itemPhoto.getId()); 
		for (int i=0;i<wrkList.size();i++) {
			item = (Items)wrkList.get(i);
			item.setPhotoid(-1);
			itemService.updateItem(item);
		}
		return true;
	}
	/*
	 * getters and setters
	 */
	public String getInfomsg() {
		return infomsg;
	}
	public void setInfomsg(String infomsg) {
		this.infomsg = infomsg;
	}
}
