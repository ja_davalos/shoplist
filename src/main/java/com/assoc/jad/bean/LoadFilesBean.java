package com.assoc.jad.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

//import org.apache.myfaces.custom.fileupload.UploadedFile;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.assoc.jad.hbm.email.EmailController;
import com.assoc.jad.hbm.items.Items;
import com.assoc.jad.hbm.photos.ItemPhotos;
import com.assoc.jad.hbm.shoplist.Shoplist;
import com.assoc.jad.hbm.shoplist.ShoplistController;
import com.assoc.jad.hbm.stores.StoreController;
import com.assoc.jad.tools.PersonalFileItems;
import com.assoc.jad.tools.ShopListStatic;

@ManagedBean@Scope(value = "session")
@Component(value = "loadFilesBean")
@ELBeanName(value = "loadFilesBean")
@Transactional
public class LoadFilesBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Autowired
	ShoplistController shoplistController;
	@Autowired
	StoreController storeController;
	@Autowired
	EmailController emailController;
	@Autowired
	ItemPhotoBean itemPhotobean;
	
	private UploadedFile uploadedFile;
	private String mainPhoto;
	private String infomsg;
	private Shoplist shoplist;
	private MasterListBean masterListBean = null;
	private String email;
	
	private boolean itHasAFamily() {
		infomsg = "there is no shoplist associated with this request";
		shoplist = null;
		ExternalContext external = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest req  = (HttpServletRequest) external.getRequest();
		masterListBean = (MasterListBean)req.getSession().getAttribute("masterListBean");
		FamilyBean familyBean = (FamilyBean)req.getSession().getAttribute("familyBean");
		if (masterListBean == null) {
			masterListBean = new MasterListBean();
			req.getSession().setAttribute("masterListBean",masterListBean);
		}
		if (masterListBean.getShoplist() == null) {
			if (familyBean != null)
				masterListBean.setShoplist(familyBean.getShoplist());
		}
		shoplist = masterListBean.getShoplist();
		if (shoplist == null ) {
			if (familyBean != null) familyBean.setInfomsg(infomsg);
			ShopListStatic.redirect(ShopListStatic.SELECTFAMILY);
			return false;
		}
		infomsg = "";
		return true;
	}	
	private boolean itHasEmail() {
		infomsg = "there is no Email associated with this request";
		ExternalContext external = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest req  = (HttpServletRequest) external.getRequest();
		FamilyBean familyBean = (FamilyBean)req.getSession().getAttribute("familyBean");
		if (familyBean != null) {
			this.email = familyBean.getEmail();
		}
		if (email == null ) return false;
		infomsg = "";
		return true;
	}	
	private String getHTMLParam(String name) {
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext external = context.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) external.getRequest();
		return request.getParameter(name);
	}
	public void uploadFile(FileUploadEvent event) {
		uploadedFile = event.getFile();
		if (!itHasEmail()) return;
		if (uploadedFile == null || !itHasAFamily()) return;

		PersonalFileItems pfc = new PersonalFileItems(uploadedFile);
		HashMap<String, List<String>> storesLists = pfc.getStoreShopList();

		if (!storeController.initStore(shoplist)) {//shoplist does not exist in DB
			ShopListStatic.redirect(ShopListStatic.SELECTFAMILY);
			return;
		} else {
			for (String key: storesLists.keySet()) {
				storeController.addList(key,storesLists.get(key));
			}
			storeController.loadAllStores();
			masterListBean.setStoreShopList(storeController.getStoreShopList());
		}
	}
	public void uploadItemPhoto(FileUploadEvent event) {
		uploadedFile = event.getFile();
		if (uploadedFile == null || !itHasAFamily()) return;
		JSONObject jsonitem=null;
		try {
			jsonitem = (JSONObject) new JSONParser().parse(getHTMLParam("jsonitem"));
		} catch (ParseException e) {
			throw new RuntimeException("Unable to parse json " + jsonitem);
		}
		String photoname = uploadedFile.getFileName();
		int ndx = photoname.lastIndexOf('.');
		String ext = "";
		String itemName = (String)jsonitem.get("name");
		if (ndx != -1 ) ext = photoname.substring(++ndx);
		photoname = itemName+"."+ext;
		//ItemPhotoBean itemPhotobean = new ItemPhotoBean(shoplist);
		ItemPhotos photo = itemPhotobean.updatePhoto(uploadedFile, photoname,shoplist.getId());
		if (photo == null) {
			masterListBean.setInfomsg(itemPhotobean.getInfomsg());
			ShopListStatic.redirect(ShopListStatic.STORELIST);
			return;
		}
		
		HashMap<String, HashMap<String, Object>> familySHopList = masterListBean.getStoreShopList();
		storeController.initStore(shoplist);
		for (String keyStore: familySHopList.keySet()) {
			HashMap<String, Object> storeItems = familySHopList.get(keyStore);
				Items item = (Items) storeItems.get(itemName);
				if (item == null) continue;
				item.setPhotoid(photo.getId());
				if (!storeController.updateStoreItem(item)) {
					masterListBean.setInfomsg(storeController.getInfomsg());
					ShopListStatic.redirect(ShopListStatic.STORELIST);
					return;
				}
		}
		masterListBean.setInfomsg("successfully added photo to database");
		ShopListStatic.redirect(ShopListStatic.STORELIST);
	}
	public void upload() {
        if(this.uploadedFile != null) {
            FacesMessage message = new FacesMessage("Succesful", uploadedFile.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }
    public void handleFileUpload(FileUploadEvent event) {
        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
/*
 * getters and setters
 */
	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}
	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}
	public String getMainPhoto() {
		return mainPhoto;
	}
	public void setMainPhoto(String mainPhoto) {
		this.mainPhoto = mainPhoto;
	}
	public String getInfomsg() {
		return infomsg;
	}
	public void setInfomsg(String infomsg) {
		this.infomsg = infomsg;
	}
}
