cd C:\Program Files\MySQL\MySQL Server 5.5\bin
mysql  -u root -pJorgeDavalos < C:\jadProject\jsfcoop\database\buildCoopDB.sql
mysql  -u root -pJorgeDavalos coopdb < C:\jadProject\jsfcoop\database\categories.sql

mysqldump -u root -pJorgeDavalos coopdb > D:\coopdb.sql
mysql -u root -pJorgeDavalos coopdb < D:\coopdb.sql

postgrep -U postgres -pJorgeDavalos superuser
postgrep -U postgres -pMayo0507 //window 32 run pg_env.bat to set userid.
in psql: \COPY homenets FROM 'C:/jadallfolders/jadJavaE64/homenet/database/coops.txt'

mysql -u root -pJorgeDavalos coopdb < D:\coopdb.sql
use CONTACTS;
show tables;
show databases;

CREATE DATABASE coopDB;

drop TABLE FRIENDS/ORGANIZATIONS
delete from coops where id=4;

LOAD DATA LOCAL INFILE 'C:/jadProject/jsfcoop/database/coops.txt' INTO TABLE COOPS LINES TERMINATED BY '\r\n';
LOAD DATA LOCAL INFILE 'C:/jadProject/jsfcoop/database/users.txt' INTO TABLE users LINES TERMINATED BY '\r\n';
#LOAD DATA LOCAL INFILE 'C:/jadProject/jsfcoop/database/categories.txt' INTO TABLE categories LINES TERMINATED BY '\r\n';

update FRIENDS set lastname = 'O\'Connor'
where firstname = 'Fiona'

CREATE TABLE coops (
id bigint(20) unsigned NOT NULL auto_increment,
name varchar(120) NOT NULL default '',
address varchar(120) NOT NULL default '',
city varchar(120) NOT NULL default '',
state varchar(30) NOT NULL default '',
zipcode varchar(12) NOT NULL default '',
country varchar(50) NOT NULL default '',
description text,
createdate datetime NOT NULL default '2010-05-07 00:00:00',
email varchar(120) not null default '',
categories varchar(120) NOT NULL default '',
PRIMARY KEY (id) ) 

// categories is a list of integer separated by a space(s)
// materials  is a list of integer separated by a space(s)
CREATE TABLE products (
id bigint(20) unsigned NOT NULL auto_increment,
coopid bigint(20) unsigned NOT NULL default '0',
name varchar(120) NOT NULL default '',
categories varchar(120) NOT NULL default '',
materials varchar(120) NOT NULL default '',
photoname varchar(120) NOT NULL default '',
price decimal(15,2) NOT NULL default 0.0,
description text NOT NULL,
PRIMARY KEY (id),
KEY master_idx (coopid) ) //TYPE=InnoDB

CREATE TABLE photos (
id bigint(20) unsigned NOT NULL auto_increment,
productid bigint(20) unsigned NOT NULL default '0',
name varchar(120) NOT NULL default '',
userid  varchar(20) NOT NULL default '',
createdate datetime NOT NULL default '2010-05-07 00:00:00',
photo blob NOT NULL,
PRIMARY KEY (id),
KEY master_idx (productid) ) TYPE=InnoDB

//documents related to coops
CREATE TABLE documents (
id bigint(20) unsigned NOT NULL auto_increment,
coopid bigint(20) unsigned NOT NULL default '0',
name varchar(120) NOT NULL default '',
userid  varchar(20) NOT NULL default '',
createdate datetime NOT NULL default '2010-05-07 00:00:00',
document blob NOT NULL,
PRIMARY KEY (id),
KEY master_idx (coopid) ) TYPE=InnoDB

// type=COOP/PRODUCT
CREATE TABLE categories (
id bigint(20) unsigned NOT NULL auto_increment,
type varchar(10) NOT NULL default '',
name varchar(120) NOT NULL default '',
PRIMARY KEY (id) ) TYPE=InnoDB

CREATE TABLE materials (
id bigint(20) unsigned NOT NULL auto_increment,
name varchar(120) NOT NULL default '',
PRIMARY KEY (id) ) TYPE=InnoDB

// coopid  is a list of integer separated by a space(s)
CREATE TABLE users (
firstname varchar(120) NOT NULL default '',
lastname varchar(120) NOT NULL default '',
coopid varchar(120) NOT NULL default '',
userid  varchar(20) NOT NULL default '',
password  varchar(120) NOT NULL default '',
type  varchar(20) NOT NULL default '',
email varchar(120) not null default '',
failedlogin int not null default 0,
createdate datetime NOT NULL default '2010-05-07 00:00:00',
PRIMARY KEY (userid) ) TYPE=MyISAM

//Customer one to one/many orders
CREATE TABLE customers (
id bigint(20) unsigned NOT NULL auto_increment,
firstname varchar(120) NOT NULL default '',
lastname varchar(120) NOT NULL default '',
email varchar(120) NOT NULL default '',
phone varchar(120) NOT NULL default '',
deliveryaddress varchar(120) NOT NULL default '',
orderid bigint(20) unsigned NOT NULL default 0,
PRIMARY KEY (id) ) TYPE=InnoDB

//one order is made up of may order details.
CREATE TABLE orders (
id bigint(20) unsigned NOT NULL auto_increment,
customerid bigint(20) unsigned NOT NULL default '0',
customerstatus int not null default 0,
orderstatus int not null default 0,
createdate datetime NOT NULL default '2010-05-07 00:00:00',
email varchar(120) NOT NULL default '',
phone varchar(120) NOT NULL default '',
deliveryaddress varchar(120) NOT NULL default '',
PRIMARY KEY (id),
KEY master_idx (customerid) ) TYPE=InnoDB

CREATE TABLE orderdetails (
id bigint(20) unsigned NOT NULL auto_increment,
orderid bigint(20) unsigned NOT NULL default '0',
productid bigint(20) unsigned NOT NULL default '0',
coopstatus int not null default 0,
createdate datetime NOT NULL default '2010-05-07 00:00:00',
PRIMARY KEY (id),
KEY master_idx (orderid) ) TYPE=InnoDB

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ALTER TABLE coops ADD email varchar(120) NOT NULL default '' AFTER createdate;
ALTER TABLE coops ADD categories varchar(120) NOT NULL default '' AFTER email;
ALTER TABLE songmetadata 
	ADD user varchar(120) NOT NULL default '' AFTER id
ALTER TABLE productdata 
	CHANGE masterid bigint(20) NOT NULL default '0'
ALTER TABLE products DROP mainphotoname
	
select distinct cdname from songmetadata;
insert into users (firstname,lastname,coopid,userid,password)
value('jorge','Davalos','2','JADDEV','JADDEV')

UPDATE coops SET createdate='2010-05-07' WHERE id=2;

=========================================================================
SELECT k.column_name
FROM information_schema.table_constraints t
JOIN information_schema.key_column_usage k
USING(constraint_name,table_schema,table_name)
WHERE t.constraint_type='PRIMARY KEY'
  AND t.table_schema='db'
  AND t.table_name='tbl';
=============================================================================
select coops.name,max(products.id) from coops,products where coops.id=products.coopid;
select coops.name,products.id,photos.name 
 from coops,products,photos 
 where coops.id=products.coopid and photos.productid=products.id
 group by coops.id;
select coops.name,coops.id from coops 
 union (select products.name,products.coopid from products order by products.id)
 order by coops.id;
 